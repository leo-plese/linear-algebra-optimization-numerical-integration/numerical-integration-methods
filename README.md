Numerical Integration Methods.

Topics:

- Runge-Kutta 4th Order method

- Trapeze method

- Euler method

- Backward Euler method

- Predictor-corrector methods (P - predictor, C - corrector): P (EC)^2 E (P = Euler, C = Backward Euler), P EC E (P = Euler, C = Trapeze)

Implemented in Python using Matrix class developed before.

My lab assignment in Computer Aided Analysis and Design, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2021
