Programsko okruženje: PyCharm 2019.2.4
Programski jezik: Python 3.8

Upute za pokretanje:
U programskom okruženju se pokrene glavni program (bez argumenata) smješten u datoteci "main.py" sa Shift+F10 ili na tipku "Run 'main'".
Ulazni parametri za numeričke metode u svakom zadatku određeni su u datotekama:
-"zad{i}_params.txt" (i = 1,2,3,4) za parametre: T (period integracije), tmax (vrijeme do kojeg se sustav promatra), print_iter (broj iteracije nakon koje se ispisuje vrijednosti varijabli stanja - vrijedi za svaku iteraciju koja je iter % print_iter == 0).
-"zad{i}_a.txt", "zad{i}_b.txt", zad{i}_x0.txt" (i = 1,2,3,4) za parametre: a (matrica sustava A), b (matrica pobude B), x0 (vrijednosti varijabli stanja u početnom trenutku promatranja sustava (t=0)).

Pokretanjem se izvršavaju svi zadaci iz vježbe.

Rezultat svakog zadatka su:
- vrijednosti varijabli stanja u svakoj iteraciji određenoj s print_iter što se ispisuje na ekran i zapisuje u datoteke "zad{i}_e.out", "zad{i}_oe.out", "zad{i}_t.out", "zad{i}_rk4.out", "zad{i}_pec2e_p.out", "zad{i}_pec2e_c.out", "zad{i}_pece_p.out", "zad{i}_pece_c.out" (i = 1,2,3,4) koje redom predstavljaju vrijednosti varijabli stanja kroz iteracije 
numeričke metode Eulera (e), obrnutog Eulera (oe), trapeza (t), Runge-Kutta 4. reda (rk4), P (EC)^2 E postupka gdje je P=Euler i C=obrnuti Euler (zasebno za postupak P i za C) i P (EC) E postupka gdje je P=Euler i C=trapez (zasebno za postupak P i za C).
- grafovi koji prikazuju vrijednosti varijabli stanja spremljeni u datoteku graphs.pdf. Ovdje ima i dodatnih grafova za eksperimente provedene s većim tmax radi daljnje potvrde rada implementacija numeričkih metoda.

Specifično za 1. zadatak se nakon izvođenja svake pojedine metode i još na kraju zadatka sumarno prikažu akumulirane greške izračunate kao zbroj apsolutnih razlika točne vrijednosti i vrijednosti aproksimirane pojedinom numeričkom metodom zasebno za svaku varijablu stanja radi usporedbe postupaka.