from matrix import Matrix
from math import sin, cos


def numeric_method(numeric_fun, res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=False,
                   analytic_equation=None):
    t = 0
    x = x0

    print(f"x0 (t = {t})")
    x.transpose().print_matrix()
    x.transpose().print_matrix_to_file(res_file)

    num_iter = int(tmax / T) + 1

    x_accum_err = Matrix(x0.n_row, 1, [[0] for _ in range(x0.n_row)])
    for i in range(1, num_iter):
        t = i * T

        do_print = (i % print_iter == 0)

        x = numeric_fun(A, B, T, r_fun, t, x)

        if do_print:
            print()
            print("t = %.4f..." % t)
            x.transpose().print_matrix()
            x.transpose().print_matrix_to_file(res_file)

        if calc_error:
            analytic_sol = analytic_equation(t) * x0

            x_err = abs(x - analytic_sol)

            x_accum_err += x_err

    return x, x_accum_err


def calculate_error(analytic_equation, t, x0, x):
    analytic_sol = analytic_equation(t) * x0

    x_err = abs(x - analytic_sol)

    return x_err


def euler_fun(A, B, T, r_fun, t, x):
    x = x + (A * x + B * r_fun(t)).scale(T)
    return x


def euler(res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=False, analytic_equation=None):
    return numeric_method(euler_fun, res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error, analytic_equation)


def inverse_euler_fun(A, B, T, r_fun, t, x, x0=None, estimate=None):
    if estimate is None:
        id_mat = Matrix.get_identity_matrix(x.n_row)

        P = ~(id_mat - A.scale(T))
        Q = (P * B).scale(T)
        x = P * x + Q * r_fun(t)
    else:
        x = x0 + (A * estimate + B * r_fun(t)).scale(T)

    return x


def inverse_euler(res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=False, analytic_equation=None):
    return numeric_method(inverse_euler_fun, res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error,
                          analytic_equation)


def trapeze_fun(A, B, T, r_fun, t, x, x0=None, estimate=None):
    t_fut = t + T

    if estimate is None:
        id_mat = Matrix.get_identity_matrix(x.n_row)

        A_scaled_T_half = A.scale(T / 2)
        P = ~(id_mat - A_scaled_T_half)
        Q = (id_mat + A_scaled_T_half)
        R = P * Q
        S = (P * B).scale(T / 2)
        x = R * x + S * (r_fun(t) + r_fun(t_fut))
    else:
        x = x0 + (A * x0 + B * r_fun(t) + A * estimate + B * r_fun(t_fut)).scale(T / 2)

    return x


def trapeze(res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=False, analytic_equation=None):
    return numeric_method(trapeze_fun, res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error, analytic_equation)


def runge_kutta_fun(A, B, T, r_fun, t, x):
    m1 = A * x + B * r_fun(t)
    m2 = A * (x + m1.scale(T / 2)) + B * r_fun(t + T / 2)
    m3 = A * (x + m2.scale(T / 2)) + B * r_fun(t + T / 2)
    m4 = A * (x + m3.scale(T)) + B * r_fun(t + T)
    x = x + (m1 + m2.scale(2) + m3.scale(2) + m4).scale(T / 6)

    return x


def runge_kutta(res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=False, analytic_equation=None):
    return numeric_method(runge_kutta_fun, res_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error,
                          analytic_equation)


def predictor_corrector(res_p_file, res_c_file, print_iter, T, tmax, A, B, x0, r_fun, predictor, corrector, s,
                        calc_error=False, analytic_equation=None):
    num_iter = int(tmax / T) + 1
    x_init = x0

    t = 0
    print(f"x0 (t = {t})")
    x0.transpose().print_matrix()
    x0.transpose().print_matrix_to_file(res_p_file)
    x0.transpose().print_matrix_to_file(res_c_file)

    total_accum_error = Matrix(x0.n_row, 1, [[0] for _ in range(x0.n_row)])

    for iter_t in range(1, num_iter):
        t = iter_t * T

        do_print = (iter_t % print_iter == 0)

        x = predictor(A, B, T, r_fun, t, x0)

        if do_print:
            print()
            print("t = %.4f..." % t)
            print("*** P ***")

            x.transpose().print_matrix()
            x.transpose().print_matrix_to_file(res_p_file)

        estimate = x

        if do_print:
            print("*** C ***")

        for i in range(s):
            x = corrector(A, B, T, r_fun, t, x, x0, estimate)
            estimate = x

            if do_print:
                x.transpose().print_matrix()

        if calc_error:
            accum_error = calculate_error(analytic_equation, t, x_init, x)
            total_accum_error += accum_error

        if do_print:
            x.transpose().print_matrix_to_file(res_c_file)

        x0 = x

    return x, total_accum_error


def load_params(params_file_prefix):
    with open(params_file_prefix + "_params.txt", "r") as params_file:
        lines = params_file.readlines()
        lines = [l.split() for l in lines]
        for l in lines:
            if l[0].strip() == "T":
                T = float(l[1].strip())
            elif l[0].strip() == "tmax":
                tmax = float(l[1].strip())
            else:  # l[0].strip() = "print_iter" - prints state variable values at each print_iter-th iter
                print_iter = int(l[1].strip())

    return T, tmax, print_iter


def load_matrices(matrices_file_prefix):
    A = Matrix.input_matrix_from_file(matrices_file_prefix + "_a")
    B = Matrix.input_matrix_from_file(matrices_file_prefix + "_b")
    x0 = Matrix.input_matrix_from_file(matrices_file_prefix + "_x0")

    return A, B, x0


def print_result_x_and_errors(x, x_accum_err=None):
    print("///////////////////////////")
    print("x =")
    x.transpose().print_matrix()

    if x_accum_err is not None:
        print("accum error =")
        x_accum_err.transpose().print_matrix()
    print("///////////////////////////")


def do_methods(task_file_prefix, print_error, print_iter, T, tmax, A, B, x0, r_fun, calc_error=False,
               analytic_equation=None):
    print()
    print("--------------- EULER ---------------")
    with open(task_file_prefix + "_e.out", "w") as e_file:
        x, x_accum_err_e = euler(e_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=calc_error,
                                 analytic_equation=analytic_equation)
    print_result_x_and_errors(x, x_accum_err_e if print_error else None)

    print()
    print("--------------- INVERSE EULER ---------------")
    with open(task_file_prefix + "_oe.out", "w") as oe_file:
        x, x_accum_err_oe = inverse_euler(oe_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=calc_error,
                                          analytic_equation=analytic_equation)
    print_result_x_and_errors(x, x_accum_err_oe if print_error else None)

    print()
    print("--------------- TRAPEZE ---------------")
    with open(task_file_prefix + "_t.out", "w") as t_file:
        x, x_accum_err_t = trapeze(t_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=calc_error,
                                   analytic_equation=analytic_equation)
    print_result_x_and_errors(x, x_accum_err_t if print_error else None)

    print()
    print("--------------- RUNGE-KUTTA (q = 4) ---------------")
    with open(task_file_prefix + "_rk4.out", "w") as rk4_file:
        x, x_accum_err_rk = runge_kutta(rk4_file, print_iter, T, tmax, A, B, x0, r_fun, calc_error=calc_error,
                                        analytic_equation=analytic_equation)
    print_result_x_and_errors(x, x_accum_err_rk if print_error else None)

    print()
    print("--------------- P (EC)2 E (P=EULER, C=INVERSE EULER) ---------------")
    with open(task_file_prefix + "_pec2e_p.out", "w") as pec2e_p_file, open(task_file_prefix + "_pec2e_c.out", "w") as pec2e_c_file:
        x, x_accum_err_pec2e = predictor_corrector(pec2e_p_file, pec2e_c_file, print_iter, T,
                                                   tmax, A, B, x0, r_fun, euler_fun, inverse_euler_fun,
                                                   s=2, calc_error=calc_error,
                                                   analytic_equation=analytic_equation)

    print_result_x_and_errors(x, x_accum_err_pec2e if print_error else None)

    print()
    print("--------------- P EC E (P=EULER, C=TRAPEZE) ---------------")
    with open(task_file_prefix + "_pece_p.out", "w") as pece_p_file, open(task_file_prefix + "_pece_c.out","w") as pece_c_file:
        x, x_accum_err_pece = predictor_corrector(pece_p_file, pece_c_file, print_iter, T,
                                                      tmax, A, B, x0, r_fun, euler_fun, trapeze_fun,
                                                      s=1, calc_error=calc_error,
                                                      analytic_equation=analytic_equation)

    print_result_x_and_errors(x, x_accum_err_pece if print_error else None)


    if print_error:
        print()
        print("-------------------------------------")
        print(":::::::: ACCUMULATED ERRORS ::::::::")
        print("EULER:")
        x_accum_err_e.print_matrix()
        print()
        print("INVERSE EULER:")
        x_accum_err_oe.print_matrix()
        print()
        print("TRAPEZE:")
        x_accum_err_t.print_matrix()
        print()
        print("RUNGE-KUTTA (q = 4):")
        x_accum_err_rk.print_matrix()
        print()
        print("P (EC)2 E (P=EULER, C=INVERSE EULER):")
        x_accum_err_pec2e.print_matrix()
        print()
        print("P EC E (P=EULER, C=TRAPEZE):")
        x_accum_err_pece.print_matrix()
        print()
        print("-------------------------------------")


def print_input_params(A, B, x0, T, tmax):
    print("A")
    A.transpose().print_matrix()
    print("B")
    B.transpose().print_matrix()
    print("x0")
    x0.transpose().print_matrix()

    print(f"T = {T} , tmax = {tmax}")


def zad1():
    print("********************** ZAD 1 **********************")
    A, B, x0 = load_matrices("zad1")
    T, tmax, print_iter = load_params("zad1")
    print_input_params(A, B, x0, T, tmax)

    analytic_formula = lambda t: Matrix(2, 2, [[cos(t), sin(t)], [-sin(t), cos(t)]])
    r_fun = lambda t: Matrix(2, 1, [[1], [1]])

    do_methods("zad1", True, print_iter, T, tmax, A, B, x0, r_fun, calc_error=True, analytic_equation=analytic_formula)


def zad2():
    print()
    print("********************** ZAD 2 **********************")
    A, B, x0 = load_matrices("zad2")
    T, tmax, print_iter = load_params("zad2")
    print_input_params(A, B, x0, T, tmax)

    r_fun = lambda t: Matrix(2, 1, [[1], [1]])

    do_methods("zad2", False, print_iter, T, tmax, A, B, x0, r_fun)

    print(
        ":::::::::::::: RUNGE-KUTTA (q = 4) with T = 0.0278 (max possible T (for convergence) for precision of 4 decimals) ::::::::::::::")
    with open("zad2_rk4_cvg.out", "w") as rk4_cvg_file:
        x, x_accum_err = runge_kutta(rk4_cvg_file, print_iter, 0.0278, 100, A, B, x0, r_fun)
    print_result_x_and_errors(x)


def zad3():
    print()
    print("********************** ZAD 3 **********************")
    A, B, x0 = load_matrices("zad3")
    T, tmax, print_iter = load_params("zad3")
    print_input_params(A, B, x0, T, tmax)

    r_fun = lambda t: Matrix(2, 1, [[1], [1]])

    do_methods("zad3", False, print_iter, T, tmax, A, B, x0, r_fun)


def zad4():
    print()
    print("********************** ZAD 4 **********************")
    A, B, x0 = load_matrices("zad4")
    T, tmax, print_iter = load_params("zad4")
    print_input_params(A, B, x0, T, tmax)

    r_fun = lambda t: Matrix(2, 1, [[t], [t]])

    do_methods("zad4", False, print_iter, T, tmax, A, B, x0, r_fun)


if __name__ == "__main__":
    zad1()
    zad2()  # RK: T = 0.0278 OK (cvg) (na 4 decimale odredeno); T = 0.0279 vise nije OK (dvg!)
    zad3()
    zad4()
