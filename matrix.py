class Matrix():
    EPS = 1e-9

    # (row count, column count, elements list)
    def __init__(self, n_row, n_col, elements):

        self.n_row = n_row
        self.n_col = n_col
        self.elements = elements

    @staticmethod
    def get_number_of_matrix_rows_columns(rc_str):
        while True:
            try:
                n_rc = int(input("N " + rc_str + " = "))
                while n_rc < 1:
                    print("Number of " + rc_str + " must be at least 1.")
                    n_rc = int(input("N " + rc_str + " = "))
            except:
                print("Number of " + rc_str + " must be integer.")
            else:
                return n_rc

    @staticmethod
    def input_matrix_from_console():
        n_row = Matrix.get_number_of_matrix_rows_columns("rows")
        n_col = Matrix.get_number_of_matrix_rows_columns("columns")

        elements = []
        for i in range(1, n_row + 1):
            row_new = []

            while True:
                row_input = input("Enter row %5d:" % i).strip().split()
                if len(row_input) != n_col:
                    print(f"Entered row does not have {n_col} columns. Enter this row again.")
                    continue

                invalid_input = False
                for el in row_input:
                    try:
                        row_new.append(float(el))
                    except:
                        print(f"Matrix elements must be real numbers (element '{el}'). Enter this row again.")
                        invalid_input = True
                        break
                if not invalid_input:
                    break

            elements.append(row_new)

        return n_row, n_col, elements

    @staticmethod
    def input_matrix_from_file(mat_file_name):
        mat_file_name += ".txt"
        try:
            with open(mat_file_name, "r") as mat_file:
                mat_rows_in = mat_file.readlines()
        except FileNotFoundError:
            raise FileNotFoundError("File '" + mat_file_name + "' not found.")

        if len(mat_rows_in) == 0:
            raise ValueError("Matrix is empty")

        mat_rows = []
        for r in mat_rows_in:
            mat_rows.append(r.strip().split())

        n_col = len(mat_rows[0])
        for r in mat_rows:
            if len(r) != n_col:
                raise ValueError("Matrix rows are of different length")

        if n_col == 0:
            raise ValueError("Matrix rows are empty")

        n_row = len(mat_rows)

        for i in range(n_row):
            r = mat_rows[i]
            for j in range(n_col):
                try:
                    r[j] = float(r[j])
                except:
                    raise ValueError(f"Matrix elements must be real numbers (element '{r[j]}').")

        return Matrix(n_row, n_col, mat_rows)

    def print_matrix(self):
        for r in self.elements:
            for el in r:
                print("%.10f" % el, end=" ")
            print()

    def print_matrix_to_file(self, mat_file):
        row_strs = []

        for r in self.elements:
            row_strs.append(' '.join(map(str, r)) + "\n")

        mat_file.writelines(row_strs)

    def get_row_col_index(self, row_in, col_in):
        try:
            row = int(row_in)
        except:
            raise ValueError("Index of row must be integer.")

        try:
            col = int(col_in)
        except:
            raise ValueError("Index of column must be integer.")

        if row < 0 or row >= self.n_row:
            raise ValueError(f"Index of row must be from 0 to {self.n_row - 1}")

        if col < 0 or col >= self.n_col:
            raise ValueError(f"Index of column must be from 0 to {self.n_col - 1}")

        return row, col

    def set_entry(self, row_in, col_in, entry_in):
        row, col = self.get_row_col_index(row_in, col_in)

        try:
            entry = float(entry_in)
        except:
            raise ValueError("Matrix entry must be real number.")

        self.elements[row][col] = entry

    def get_entry(self, row_in, col_in):
        row, col = self.get_row_col_index(row_in, col_in)

        return self.elements[row][col]

    # self + other
    def __add__(self, other):
        if self.n_row != other.n_row or self.n_col != other.n_col:
            raise ValueError("Matrices of incompatible dimensions for sum")

        res_elements = []
        for i in range(self.n_row):
            res_row = []
            for j in range(self.n_col):
                res_row.append(self.get_entry(i, j) + other.get_entry(i, j))
            res_elements.append(res_row)

        return Matrix(self.n_row, self.n_col, res_elements)

    # self - other
    def __sub__(self, other):
        if self.n_row != other.n_row or self.n_col != other.n_col:
            raise ValueError("Matrices of incompatible dimensions for subtraction")

        res_elements = []
        for i in range(self.n_row):
            res_row = []
            for j in range(self.n_col):
                res_row.append(self.get_entry(i, j) - other.get_entry(i, j))
            res_elements.append(res_row)

        return Matrix(self.n_row, self.n_col, res_elements)

    # self * other (matrix product)
    def __mul__(self, other):
        if self.n_col != other.n_row:
            raise ValueError("Matrices of incompatible dimensions for matrix multiplication")

        res_elements = []
        for i in range(self.n_row):
            res_row = []
            for j in range(other.n_col):
                elem = 0
                for k in range(self.n_col):
                    elem += self.get_entry(i, k) * other.get_entry(k, j)
                res_row.append(elem)
            res_elements.append(res_row)

        return Matrix(self.n_row, other.n_col, res_elements)

    # self.T
    def transpose(self):
        res_elements = [[None for j in range(self.n_row)] for i in range(self.n_col)]

        for i in range(self.n_row):
            for j in range(self.n_col):
                res_elements[j][i] = self.elements[i][j]

        return Matrix(self.n_col, self.n_row, res_elements)

    # abs(self)
    def __abs__(self):
        res_elements = [[None for j in range(self.n_col)] for i in range(self.n_row)]

        for i in range(self.n_row):
            for j in range(self.n_col):
                res_elements[i][j] = abs(self.elements[i][j])

        return Matrix(self.n_row, self.n_col, res_elements)

    # self = self + other
    def __iadd__(self, other):
        if self.n_row != other.n_row or self.n_col != other.n_col:
            raise ValueError("Matrices of incompatible dimensions for sum")

        for i in range(self.n_row):
            for j in range(self.n_col):
                self.set_entry(i, j, self.get_entry(i, j) + other.get_entry(i, j))

        return self

    # self = self - other
    def __isub__(self, other):
        if self.n_row != other.n_row or self.n_col != other.n_col:
            raise ValueError("Matrices of incompatible dimensions for subtraction")

        for i in range(self.n_row):
            for j in range(self.n_col):
                self.set_entry(i, j, self.get_entry(i, j) - other.get_entry(i, j))

        return self

    def __eq__(self, other):
        if self.n_row != other.n_row or self.n_col != other.n_col:
            raise ValueError("Matrices of incompatible dimensions for comparison")

        for i in range(self.n_row):
            for j in range(self.n_col):
                if abs(self.get_entry(i, j) - other.get_entry(i, j)) > Matrix.EPS:
                    # if self.get_entry(i, j) != other.get_entry(i, j):      # ovo nije u redu
                    return False

        return True

    def scale(self, scale_factor_in):
        try:
            scale_factor = float(scale_factor_in)
        except ValueError:
            raise ValueError("Scale factor must be real number")

        res_elements = []
        for r in self.elements:
            res_elements.append([scale_factor * el for el in r])

        return Matrix(self.n_row, self.n_col, res_elements)

    def get_sum_of_elements(self):
        nc = self.n_col
        nr = self.n_row
        return sum([self.elements[i][j] for j in range(nc) for i in range(nr)])


    ########################################################################################

    def check_entry_if_zero(self, entry, method_descr, error_descr):
        if (abs(entry) < Matrix.EPS):
            raise ValueError(method_descr + ": " + error_descr + "!")

    def is_square(self):
        return self.n_row == self.n_col

    # self = L (lower triangular) matrix (or entire A matrix), b = free vector
    def substitute_forward(self, b):
        if not self.is_square():
            raise ValueError("Matrix L is not square matrix. Cannot do forward substitution.")
        if not (b.n_row == self.n_row and b.n_col == 1):
            raise ValueError("Vector b is not of proper dimensions.")

        n = self.n_row

        b_elements = b.elements
        for i in range(0, n - 1):
            for j in range(i + 1, n):
                b_elements[j][0] -= self.get_entry(j, i) * b.get_entry(i, 0)

        y_elements = b_elements

        return Matrix(b.n_row, 1, y_elements)

    # self = U (upper triangular) matrix (or entire A matrix), y
    def substitute_backward(self, y):
        if not self.is_square():
            raise ValueError("Matrix U is not square matrix. Cannot do forward substitution.")
        if not (y.n_row == self.n_row and y.n_col == 1):
            raise ValueError("Vector y is not of proper dimensions.")

        n = self.n_row

        y_elements = y.elements
        for i in range(n - 1, -1, -1):

            a_ii = self.get_entry(i, i)
            self.check_entry_if_zero(a_ii, "Backward substitution", "divison by zero")

            y_elements[i][0] /= a_ii
            for j in range(0, i):
                y_elements[j][0] -= self.get_entry(j, i) * y.get_entry(i, 0)

        x_elements = y_elements

        return Matrix(y.n_row, 1, x_elements)

    def decompose_lu(self):
        n = self.n_row

        for i in range(0, n - 1):
            for j in range(i + 1, n):
                a_ii = self.get_entry(i, i)
                self.check_entry_if_zero(a_ii, "LU decomposition", "divison by zero")

                self.elements[j][i] /= a_ii
                for k in range(i + 1, n):
                    self.elements[j][k] -= self.elements[j][i] * self.elements[i][k]

    def get_permutation_vector(self):
        return list(range(0, self.n_row))

    def choose_pivot_element(self, c):
        n = self.n_row

        cur_max_el = abs(self.get_entry(c, c))
        cur_max_ind = c
        for r in range(c, n):
            cur_el = abs(self.get_entry(r, c))
            if cur_el > cur_max_el:
                cur_max_el = cur_el
                cur_max_ind = r

        return cur_max_ind, cur_max_el

    def exchange_rows(self, i, j):
        self.elements[i], self.elements[j] = self.elements[j], self.elements[i]

    def log_change(self, p, i, j):
        p[i], p[j] = p[j], p[i]

    @staticmethod
    def create_permutation_matrix(p):
        n = len(p)
        p_mat = [[1 if i == x else 0 for i in range(n)] for x in p]

        return Matrix(n, n, p_mat)

    def decompose_lup(self):
        p = self.get_permutation_vector()

        n = self.n_row
        n_row_exchange = 0
        for i in range(0, n - 1):
            pivot_ind, pivot_el = self.choose_pivot_element(i)

            self.check_entry_if_zero(pivot_el, "LUP decomposition", "singular matrix")

            # if pivot is not current element A[i][i] -> exchange rows + log change in permutation vector
            if pivot_ind != i:
                self.exchange_rows(pivot_ind, i)
                self.log_change(p, pivot_ind, i)

                n_row_exchange += 1

            for j in range(i + 1, n):
                a_ii = self.get_entry(i, i)
                self.check_entry_if_zero(a_ii, "LUP decomposition", "divison by zero")

                self.elements[j][i] /= a_ii
                for k in range(i + 1, n):
                    self.elements[j][k] -= self.elements[j][i] * self.elements[i][k]

        return p, n_row_exchange

    def reorder_elements(self, p):
        n = len(p)
        els = self.elements
        b_new = [els[i] for i in p]
        b = Matrix(n, 1, b_new)

        return b

    def create_unit_vectors(self):
        n = self.n_row

        I_mat = [[[1] if i == j else [0] for j in range(n)] for i in range(n)]

        unit_vectors = [Matrix(n, 1, x) for x in I_mat]

        return unit_vectors

    # ~
    def __invert__(self):
        unit_vectors = self.create_unit_vectors()

        p_vect, _ = self.decompose_lup()

        n = self.n_row

        for i in range(n):
            unit_vectors[i] = unit_vectors[i].reorder_elements(p_vect)

        inverse_mat_els = [[None for j in range(n)] for i in range(n)]

        for i in range(n):
            y_i = self.substitute_forward(unit_vectors[i])
            x_i = self.substitute_backward(y_i)

            x_i_els = x_i.elements
            for j in range(n):
                inverse_mat_els[j][i] = x_i_els[j][0]

        return Matrix(n, n, inverse_mat_els)

    def get_product_of_diagonal(self):
        prod = 1
        for i in range(self.n_row):
            prod *= self.get_entry(i, i)

        return prod

    def get_determinant(self, n_permutations):
        determinant = (-1) ** n_permutations * self.get_product_of_diagonal()

        return determinant

    @staticmethod
    def get_identity_matrix(n):
        I_mat = Matrix(n, n, [[1 if i == j else 0 for j in range(n)] for i in range(n)])

        return I_mat
